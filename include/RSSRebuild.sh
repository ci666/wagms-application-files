source /home/cms/include/logger.sh

ConfigFile=/home/cms/CMS.cfg
SiteLocation=$(awk '/SiteLocation/{print $2}' $ConfigFile)
SiteURL=$(awk '/FullURL/{print $2}' $ConfigFile)
RSSSiteName=$(awk '/RSSSiteName/{print $2}' $ConfigFile)
RSSSiteDescription=$(grep RSSSiteDescription $ConfigFile | cut -f2- -d ' ')

RSSFEED=$SiteLocation/rss.xml

#RSS Feed Generation
RSSRebuilder()
{
log "Building RSS"
echo '<?xml version="1.0" encoding="UTF-8" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
        <title>'$RSSSiteName'</title>
        <link>'$SiteURL'</link>
        <description>'$RSSSiteDescription'</description>
        <lastBuildDate>'$(date -R)'</lastBuildDate>
	<atom:link href="'$SiteURL'/rss.xml" rel="self" type="application/rss+xml"/>
	<language>en-us</language>
' > $RSSFEED

for i in $(ls $SiteLocation/posts/ | awk '/[0-9]/' | sort -r)
do
echo '
        <item>
        <title>'$(cat $SiteLocation/posts/$i | grep -v TOC | head -n1 | awk '/>.*</' | cut -d'>' -f2 | cut -d'<' -f1)'</title>
        <link>'$SiteURL'/article/'$(ls $SiteLocation/article/ | grep $i)'</link>
        <guid>'$SiteURL'/article/'$(ls $SiteLocation/article/ | grep $i)'</guid>
        <pubDate>'$(date -d "$(ls $SiteLocation/posts/ -l | grep $i | awk '{print $8}') $(cat $SiteLocation/posts/$i | awk '/--/' | cut -d- -f4 | sed -e 's/^[ \t]*//')" | awk '{print $1",  "$3" "$2" "$6" "$4" "$7" PST"}')'</pubDate>
        <description>'$(cat $SiteLocation/posts/$i | awk 'NR > 2 { print }' | sed -e :a -e 's/<[^>]*>//g;/</N;//ba' | sed 's/^[ \t]*//;s/[ \t]*$//' | sed '/^$/d')'</description>
        </item>
' >> $RSSFEED
done

echo '
</channel>
</rss>
' >> $RSSFEED

log "RSS Built"
}



