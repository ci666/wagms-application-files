source /home/cms/include/logger.sh
ConfigFile=/home/cms/CMS.cfg

SiteLocation=$(awk '/SiteLocation/{print $2}' $ConfigFile)
AppUsr=$(awk '/ApplicationUserName/{print $2}' $ConfigFile)
AppGrp=$(awk '/ApplicationGroupName/{print $2}' $ConfigFile)

FixPerms()
{
log "Running Permissions Fixer"
find $SiteLocation/ ! -user $AppUsr -exec chown $AppUsr {} \;
find $SiteLocation/ ! -group $AppGrp -exec chgrp $AppGrp {} \;
find $SiteLocation/ -type f ! -perm 644 -exec chmod 755 {} \;
find $SiteLocation/ -type d ! -perm 755 -exec chmod 755 {} \;
log "Done"
}

