source /home/cms/include/logger.sh

ConfigFile=/home/cms/CMS.cfg
SiteLocation=$(awk '/SiteLocation/{print $2}' $ConfigFile)

LOC=$(pwd)
POSTLOC=$SiteLocation/posts/
FILE=$(date +%Y%m%d)
X=01
NAME=""


FileNameCheck()
   {
        log "Searching for open filename"

        cd $POSTLOC
        while [  -e "$FILE""$(printf %02d $X)".html ]
        do
                X=$(($X+01))
                X=$(printf %02d $X)
        done
        NAME="$POSTLOC""$FILE""$X".html
        cd $LOC
        log "Found Filename" $NAME

        echo $NAME
   }
