source /home/cms/include/logger.sh  

ConfigFile=/home/cms/CMS.cfg
SiteLocation=$(awk '/SiteLocation/{print $2}' $ConfigFile)

GIT=$(which git)

GitBackup()
{
log "Starting Git Backup"
OLDLOC=$(pwd)
cd $SiteLocation
$GIT add . > /dev/null 2>&1
$GIT commit -m "CMS Reloader GIT Update - $(date)" > /dev/null 2>&1
$GIT push origin master > /dev/null 2>&1
cd $OLDLOC
log "Git Backup complete"
}

