source /home/cms/include/logger.sh
source /home/cms/include/CreateLongForm.sh

ConfigFile=/home/cms/CMS.cfg
SiteLocation=$(awk '/SiteLocation/{print $2}' $ConfigFile)


tmpTOC=$(mktemp /tmp/tmpTOC.XXXXX)
LOC=$SiteLocation/posts
TOC=$LOC/TOC.html
FULLTOC=$LOC/FULLTOC.html

RebuildTOC()
{
log "Building TOCs"

> $TOC
> $FULLTOC
rm $SiteLocation/article/*

for i in  $(ls $LOC/ | grep -v TOC | grep '.html' | sort -r)
do
        echo '<!--# include file="'$i'" -->' >> $tmpTOC
	BuildLongPost $i
done

head -n 10 $tmpTOC > $TOC

log "TOCs Built"

log "Building Archive"

for i in  $(ls $LOC/ | grep -v TOC | grep '.html' | sort -r)
do
        echo $(head -1 $LOC/$i) >> $FULLTOC
done

log "Archive Built"

rm $tmpTOC
}
rm $tmpTOC
