#!/bin/bash

source /home/cms/include/logger.sh
source /home/cms/include/TOCRebuild.sh
source /home/cms/include/FileNameCheck.sh

ConfigFile=/home/cms/CMS.cfg
SiteLocation=$(awk '/SiteLocation/{print $2}' $ConfigFile)
SiteURL=$(awk '/FullURL/{print $2}' $ConfigFile)

RequiredPrograms=( bash fetchmail perl git mail ripmime gpg awk sed )

Init()
  {
	log "Initializing"
	
	Bool="Pass"

	for i in "${RequiredPrograms[@]}"
	do
		hash $i &> /dev/null
		if [ $? -eq 1 ]; then
		    log "$i not found. Please Install it."
		    Bool="Fail"
		fi
	done
	if [ $Bool == "Fail" ]
	then
		log "Critical Failure: Missing Required Programs. Exiting"
		exit 1
	else
		log "Init Complete"
	fi
  }


SenderCheck()
  {
	tmpfile=$(mktemp)
	cat - > $tmpfile

	if [[ ! -s $tmpfile ]] ; then 
		log "Error-SenderCheck: No data Recieved. Exiting"
		exit 2
	fi

	Sender=$(cat $tmpfile | awk '/From:/{print}' | grep @ | grep '<' | awk -F "<" '{print $2}' | sed 's/>//')
	log "Message Received from" $Sender

	for i in $(cat $ConfigFile | awk '/AllowedEmail/{print $2}')
	do
        	if [ $i == $Sender ]
        	    then Passed = 1
		fi
	done

	if [ $Passed == 0 ]
	then 
		log "Invalid Sender:" $Sender "Exiting."
		log "=========================="
		#Not sending notification email here as it can be used to spam if address is spoofed
		rm $tmpfile 
		exit 3
	fi 
	log "Sender Valid"
	cat $tmpfile
	rm $tmpfile
   }


GPGCheck()
{
        tmpfile=$(mktemp)
        touch $tmpfile
	cat - > $tmpfile

	if [[ ! -s $tmpfile ]] ; then 
		log "Error-GPGCheck: No data Recieved. Exiting"
		exit 4
	fi

        gpgfile=$(mktemp)
	touch $gpgfile
        sed -n '/BEGIN PGP/,/END PGP/p' $tmpfile > $gpgfile

        log "Looking for GPG Block"

        GPGType=$(head -n1 $gpgfile | awk '/BEGIN PGP MESSAGE/{Type=1} /BEGIN PGP SIGNED MESSAGE/{Type=2} {print Type}')

        if [ -z  $GPGType ]
           then
                GPGType=0
        fi

        if [ $GPGType == 1 ]
           then
                {
                 log "Found GPG Block"
                 log "Detected Encrypted Message"

                 log "Attempting to Decrypt Message"

		 rm $decryptedfile > /dev/null 2>&1
		 rm $gpgoutputfile > /dev/null 2>&1
                 
		 decryptedfile=$(mktemp)
                 gpgoutputfile=$(mktemp)
        	 touch $decryptedfile
        	 touch $gpgoutputfile
		 
                 /usr/bin/gpg -o $decryptedfile -d $gpgfile > $gpgoutputfile 2>&1

                 SenderConfirmed=$(grep -i $(cat $tmpfile | awk '/From:/{print}' | awk -F "<" '{print $2}' | sed 's/>//') $gpgoutputfile | awk '{print $2}' )
	
                 if [ -z $SenderConfirmed ]
                    then
                        {
                         log "Decryption Failed - Exiting"
                         rm $tmpfile
                         rm $decryptedfile > /dev/null 2>&1
                         rm $gpgoutputfile > /dev/null 2>&1
                         exit 5
                        }
                 fi

                 log "Decrypted - Running again to check for GPG Signature"
		 cat $tmpfile | awk -v new=$decryptedfile '/BEGIN PGP MESSAGE/{f=1;while (getline < new ){print}}/END PGP MESSAGE/{f=0}!f' | grep -v 'END PGP MESSAGE' | GPGCheck
                }
        
        elif [ $GPGType == 2 ]
           then
                {
                 log "Found GPG Signature Block"
                 log "Checking Signature"
		
		 rm $decryptedfile > /dev/null 2>&1
		 rm $gpgoutputfile > /dev/null 2>&1
                 decryptedfile=$(mktemp)
                 gpgoutputfile=$(mktemp)
        	 touch $decryptedfile
        	 touch $gpgoutputfile
		
                 /usr/bin/gpg -o $decryptedfile -d $gpgfile > $gpgoutputfile 2>&1

                 Sigcheck=$(awk '/Good signature/{print 1}' $gpgoutputfile)
                 if [ -z $Sigcheck ]
                 then
                        log "Bad GPG Signature - Exiting"
                        rm $decryptedfile > /dev/null 2>&1
                        rm $tmpfile
                        rm $gpgfile > /dev/null 2>&1
                        rm $gpgoutputfile > /dev/null 2>&1
                        exit 6
                 fi
                
		 log "Valid GPG Signature"
		 #cat $decryptedfile >> $tmpfile
		 cat $tmpfile | awk -v new=$decryptedfile '/BEGIN PGP SIGNED MESSAGE/{f=1;while (getline < new ){print}}/END PGP SIGNATURE/{f=0}!f' | grep -v 'END PGP SIGNATURE'
		 rm $decryptedfile > /dev/null 2>&1
       		 rm $tmpfile
                }
        else 
                log "Failed to Find GPG Block in Message. Notifying and Exiting."
                log "=========================="
                #echo FAIL > mail -s "'Subject: Failed to find GPG Block in message on' $(date)" $Sender 
                rm $decryptedfile > /dev/null 2>&1
                rm $gpgfile > /dev/null 2>&1
                rm $gpgoutputfile > /dev/null 2>&1
                rm $tmpfile > /dev/null 2>&1
		exit 7

        fi
        
        if [ -z $decryptedfile ]
        then
                rm $tmpfile
                exit 8    

	fi      
}

PostValidity()
   {
	tmpfile=$(mktemp)
	touch $tmpfile
	cat - > $tmpfile

	if [[ ! -s $tmpfile ]] ; then 
		log "Error-PostValidity: No data Recieved. Exiting"
		exit 9
	fi

        log "Looking for <START> and <END> Blocks"
	cleartmp=$(mktemp)
	touch $cleartmp 
	sed -n '/<START>/,/<END>/p' $tmpfile > $cleartmp
	if [[ ! -s $cleartmp ]]
	then
		log "Failed to Find <START> and <END> Blocks in Message. Notifying and Exiting."
		log "=========================="
		#echo 'FAIL' > mail -s "'Subject: Failed to find <START> and <END> Blocks in message on' $(date)" $Sender
		rm $tmpfile 
		rm $cleartmp		
		exit 10
	fi 
	log "Found <START> and <END> Block"
	cat $tmpfile
	cat $tmpfile > tmp
	rm $cleartmp		
    	rm $tmpfile
 
  }

EmailPostBuilder()
   {
	tmpfile=$(mktemp)
	touch $tmpfile
	cat - > $tmpfile

	if [[ ! -s $tmpfile ]] ; then 
		log "Error-EmailPostBuilder: No data Recieved. Exiting"
		exit 11
	fi

	NAME=$(FileNameCheck)
	log "Creating Post" 
	Link=$SiteURL/article/L$(echo $NAME | cut -d/ -f6)
	touch $NAME
	Subject=$( cat $tmpfile | awk '/Subject/' | cut -d: -f2 | sed -e 's/^[ \t]*//' ) 
	echo '-- <a href='$Link'>'$Subject'</a> -' $(date '+%B %d %G') >> $NAME 
	echo '======================================' >> $NAME
	mdpst=$(mktemp)
	touch $mdpst 
 	sed -n '/<START>/,/<END>/p' $tmpfile | grep -v '<START>' | grep -v '<END>' | sed '/./,$!d' | /home/cms/include/Markdown.pl >> $mdpst
	cat $mdpst >> $NAME
	cat $NAME | sed ':a;N;$!ba;s/=\n//g' | sed 's/<p>/\n<p>/' | sed 's\=20\\'> $mdpst 
	cat $mdpst > $NAME

	log "Ripping Attachments"
	Sub2File=$(echo $Subject | sed 's/ /-/g')
	mkdir $SiteLocation/images/$Sub2File
	ripmime --no-nameless -i $tmpfile -d $SiteLocation/images/$Sub2File/
	log "Attachment Rip Complete"

	rm $mdpst
	rm $tmpfile
	log "Created Post" $NAME
   }

Main()
   {
	log "=========================="
	log "Starting Run"
	log "Receiving Mail File"
	Init
	cat - | SenderCheck | GPGCheck | PostValidity | EmailPostBuilder
	log "Calling Rebuilder" 
	/home/cms/rebuild 
	cd /home/cms
	log "Run Complete"
	log "=========================="
	log ""
   }


cat - | Main
